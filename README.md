# Guacamole in kubernetes

## Create DATABASE with HELM

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami    
helm install bitnami/postgresql postgres -f postgres-value.yaml
```

## Install guacamole

I use traefik V2 in my kubernetes cluster so you need to add manually the ingress.

You need to set database password in guacamole-value.yaml file when the database is created.

```bash
helm repo add halkeye https://halkeye.github.io/helm-charts    
helm install guacamole halkeye/guacamole  -f guacamole-values.yaml -n guacamole
kubectl apply -f route.yaml
```
